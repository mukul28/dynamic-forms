import './App.css';
import { Admin } from './screens';
function App() {
  return (
    <div className="App">
      <Admin />
    </div>
  );
}

export default App;
