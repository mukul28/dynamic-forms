import { Dropdown } from 'react-bootstrap';

export const CustomDropdown = ({ title, options, onSelect }) => {
  return (
    <Dropdown>
      <Dropdown.Toggle variant="light" id="dropdown-basic">
        {title}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {options.map((item, index) => (
          <Dropdown.Item
            key={item.id}
            value={item.field}
            onClick={() => onSelect(item)}
          >
            {item.displayName}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};
