export const TextInput = ({ placeholder, onChange, value }) => {
  return (
    <input
      type="text"
      placeholder={placeholder}
      className="Input"
      value={value}
      onChange={onChange}
    />
  );
};
