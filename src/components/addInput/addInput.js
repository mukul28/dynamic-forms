import { Fragment, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { CustomDropdown } from '../../components/dropdown/dropdown';
import { TextInput } from '../../components/textInput/textInput';
import { addData } from '../../redux/actions/form';

export function AddInput() {
  const [name, setName] = useState('');
  const [fieldType, setFieldType] = useState(null);
  const [option, setOption] = useState('');
  const [dropdownOptions, setDropdownOptions] = useState([]);

  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const { formFields } = state.formReducer;

  const onAdd = () => {
    let data = {
      name,
      fieldType,
    };
    if (dropdownOptions.length) {
      data.dropdownOptions = dropdownOptions;
    }
    dispatch(addData(data));
    setName('');
    setFieldType(null);
    setDropdownOptions([]);
  };

  const onAddOption = () => {
    setDropdownOptions([...dropdownOptions, option]);
    setOption('');
  };
  const onDeleteOption = (index) => {
    setDropdownOptions(
      dropdownOptions
        .slice(0, index)
        .concat(dropdownOptions.slice(index + 1, dropdownOptions.length))
    );
  };

  return (
    <Fragment>
      <h3>Create New</h3>
      <Row className="RowInputs">
        <Col md="8">
          <TextInput
            placeholder="Field Name"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />

          {fieldType?.field === 'dropDown' && (
            <Fragment>
              <Fragment>
                <h5 style={{ marginTop: '20px' }}>Add options</h5>
                <Row
                  style={{
                    marginTop: '10px',
                  }}
                >
                  <Col md="8">
                    <TextInput
                      placeholder="Field Name"
                      onChange={(e) => setOption(e.target.value)}
                      value={option}
                    />
                  </Col>
                  <Button
                    variant={option ? 'outline-success' : 'outline-secondary'}
                    onClick={onAddOption}
                    style={{ marginLeft: '5px' }}
                    size="sm"
                  >
                    Add
                  </Button>
                </Row>
              </Fragment>
              {dropdownOptions.map((item, index) => (
                <Fragment key={index}>
                  <Row style={{ marginTop: '20px' }}>
                    <Col md="4">
                      <Row>
                        <Col md="6">
                          <text>{item}</text>
                        </Col>
                        <Col md="6">
                          <Row>
                            <Button
                              variant={'outline-danger'}
                              onClick={() => onDeleteOption(index)}
                              style={{ marginLeft: '5px' }}
                              size="sm"
                            >
                              Delete
                            </Button>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Fragment>
              ))}
            </Fragment>
          )}
        </Col>
        <Col md="4">
          <Row>
            <Col md="6">
              <CustomDropdown
                title={fieldType ? fieldType.displayName : 'Select Type'}
                options={formFields}
                onSelect={(item) => setFieldType(item)}
              />
            </Col>
            <Col md="6" style={{ paddingLeft: 0 }}>
              <Button
                variant={
                  (name && fieldType) ||
                  (fieldType?.field === 'dropDown' && dropdownOptions.length)
                    ? 'outline-success'
                    : 'outline-secondary'
                }
                onClick={onAdd}
                disabled={
                  !name ||
                  !fieldType ||
                  (fieldType.field === 'dropDown' && !dropdownOptions.length)
                }
                size={'sm'}
              >
                Add
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <hr />
    </Fragment>
  );
}
