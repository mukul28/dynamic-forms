export const TextArea = ({ placeholder, onChange, value }) => {
  return (
    <textarea
      type="text"
      placeholder={placeholder}
      className="InputArea"
      value={value}
      onChange={onChange}
    />
  );
};
