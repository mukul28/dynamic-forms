import { Fragment } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { deleteData } from '../../redux/actions/form';

export function CreatedInput() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const { formData } = state.formReducer;

  const onDelete = (index) => {
    dispatch(deleteData(index));
  };
  return (
    <Fragment>
      {formData.length ? (
        <Fragment>
          <h3 style={{ marginTop: '20px' }}>Created Field</h3>
          <Row className="RowInputs">
            <Col md="8">
              <h5>Filed Name</h5>
            </Col>
            <Col md="4">
              <Row>
                <Col md="6">
                  <h5>Field Type</h5>
                </Col>
                <Col md="6">
                  <Row>
                    <h5>Action</h5>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Fragment>
      ) : null}
      {formData.map((item, index) => (
        <Row className="RowInputs" key={index}>
          <Col md="8">
            <text>{item.name}</text>
          </Col>
          <Col md="4">
            <Row>
              <Col md="6">
                <text>{item.fieldType.displayName}</text>
              </Col>
              <Col md="6">
                <Row>
                  <Button
                    variant={'outline-danger'}
                    onClick={() => onDelete(index)}
                    style={{ marginLeft: '5px' }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      ))}
    </Fragment>
  );
}
