import { Container } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { MyForm, CreateFrom } from '../../containers';

export function Admin() {
  const state = useSelector((state) => state);
  const { form } = state.formReducer;
  return (
    <Container
      fluid="md"
      style={{
        borderColor: 'black',
        borderWidth: '2',
      }}
    >
      <MyForm />
      {form.length ? <CreateFrom /> : null}
    </Container>
  );
}
