import { Fragment } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { CustomDynamicDropdown } from '../../components';
import { TextArea } from '../../components/textArea/textArea';
import { TextInput } from '../../components/textInput/textInput';

export function CreateFrom() {
  const state = useSelector((state) => state);
  const { form } = state.formReducer;
  const renderField = (item) => {
    switch (item.fieldType.field) {
      case 'text':
        return <TextInput placeholder={item.name} />;
      case 'textArea':
        return <TextArea placeholder={item.name} />;
      case 'dropDown':
        return (
          <CustomDynamicDropdown
            title={'Select Type'}
            options={item.dropdownOptions}
          />
        );
    }
  };

  return (
    <Fragment>
      {form.map((item, index) => (
        <Row key={index} className="RowInputs">
          <Col md="8">
            <Row style={{ alignItems: 'center' }}>
              <h6 style={{ marginRight: '10px' }}>{item.name}</h6>
              {renderField(item)}
            </Row>
          </Col>
        </Row>
      ))}
    </Fragment>
  );
}
