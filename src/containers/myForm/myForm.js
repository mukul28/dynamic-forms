import { Fragment } from 'react';
import { Button, Row } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AddInput, CreatedInput } from '../../components';
import { createForm } from '../../redux/actions/form';

export function MyForm() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const { formData } = state.formReducer;

  const onCreate = () => {
    dispatch(createForm(formData));
  };

  return (
    <Fragment>
      <AddInput />
      <CreatedInput />
      <Row className="RowInputs" style={{ justifyContent: 'center' }}>
        <Button
          variant={'primary'}
          onClick={onCreate}
          style={{ marginLeft: '5px' }}
          disabled={!formData.length}
        >
          Create Form
        </Button>
      </Row>
    </Fragment>
  );
}
