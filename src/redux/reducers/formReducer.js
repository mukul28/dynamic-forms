import { FiledData } from '../../dummyData';
import { ADD_FORM_DATA, DELETE_FORM_DATA, CREATE_FORM } from '../actionTypes';
const initialState = {
  formFields: FiledData,
  formData: [],
  form: [],
};

export const formReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FORM_DATA:
      return {
        ...state,
        formData: [...state.formData, action.payload],
      };
    case DELETE_FORM_DATA:
      let list = state.formData;
      list.splice(action.payload, 1);
      return {
        ...state,
        formData: list,
      };
    case CREATE_FORM:
      return {
        ...state,
        form: action.payload,
        formData: [],
      };
    default:
      return state;
  }
};
