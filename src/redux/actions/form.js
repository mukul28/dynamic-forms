import { ADD_FORM_DATA, CREATE_FORM, DELETE_FORM_DATA } from '../actionTypes';
export const addData = (data) => ({
  type: ADD_FORM_DATA,
  payload: data,
});

export const deleteData = (index) => ({
  type: DELETE_FORM_DATA,
  payload: index,
});

export const createForm = (index) => ({
  type: CREATE_FORM,
  payload: index,
});
