export const FiledData = [
  {
    id: 1,
    field: 'text',
    displayName: 'Text',
  },
  {
    id: 2,
    field: 'textArea',
    displayName: 'Text Area',
  },
  {
    id: 3,
    field: 'dropDown',
    displayName: 'Dropdown',
  },
];
